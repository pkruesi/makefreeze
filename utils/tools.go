package utils

import (
	"fmt"
	"log"
	"os"
	"strings"
)

func CreateBaseDirs(baseDirs string, mode os.FileMode) {
	// creates all dirs from a path recursivly
	_, err := os.Stat(baseDirs)
	if err != nil {
		if os.IsNotExist(err) {
			err = os.MkdirAll(baseDirs, mode.Perm())
			if err != nil {
				if os.IsPermission(err) {
					log.Println("mkdir:", err)
				}
				log.Println(err)
			}
		}
	}
}

func DirExists(path string) (bool, error) {
	// checks if a directory exists
	stat, err := os.Stat(path)
	if err != nil {
		return false, err
	}

	if !stat.IsDir() {
		return false, fmt.Errorf("%s is not a directory", path)
	}

	return true, nil
}

func SetCurrentSymlink(stages string, product string, productFreeze string, conf Config) {
	// sets the current symlink for each given stage

	if stages != "" {
		for _, stage := range strings.Split(stages, ",") {
			symlinkname := fmt.Sprintf("%s-%s-%s", "current", product, stage)
			symlinkpath := fmt.Sprintf("%s/%s", conf.RepoDirRoot, symlinkname)
			if _, err := os.Stat(symlinkpath); err == nil {
				if err := os.Remove(symlinkpath); err != nil {
					log.Fatal("could not remove existing symlink:", symlinkpath)
				}
			}
			realpath := fmt.Sprintf("%s/%s", conf.RepoDirRoot, productFreeze)
			os.Symlink(realpath, symlinkpath)
			fmt.Println(symlinkpath, " -> ", realpath)
		}
	}
	os.Exit(0)
}

func CheckFreezeExists(productFreeze string, conf Config) bool {
	// checks if a productFreeze with the given name in the repodir already exists

	productFreezePath := fmt.Sprintf("%s/%s", conf.RepoDirRoot, productFreeze)
	_, err := os.Stat(productFreezePath)
	if err != nil {
		return false
	}
	return true
}
