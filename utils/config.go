package utils

import (
	"encoding/json"
	"fmt"
	"io/ioutil"
	"os"
	"path/filepath"
	"strings"
)

type Config struct {
	HeadDir     string         `json:"HeadDir"`
	RepoDirRoot string         `json:"RepoDirRoot"`
}


// read the config or fail if the config is not found
func GetConfig(configFile string) Config {
	var c Config
	if strings.HasPrefix(configFile, "~/") {
		dirname, _ := os.UserHomeDir()
		configFile = filepath.Join(dirname, configFile[2:])
		fmt.Println("configFile:", configFile)
	}
	raw, err := ioutil.ReadFile(configFile)
	if err != nil {

		c.HeadDir = "/path/to/headdir"
		c.RepoDirRoot = "/path/to/repodir"

		exampleConfig, _ := json.Marshal(c)

		err := fmt.Errorf("config %s not found\nexample:\n./makeFreeze.json\n  %s\n", configFile, exampleConfig)
		fmt.Println(err)
		os.Exit(2)
	}

	json.Unmarshal(raw, &c)
	return c
}
