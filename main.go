package main

import (
	"flag"
	"fmt"
	"log"
	"os"
	"path/filepath"
	"strings"
	"sync"
	"time"

	"makeFreeze/utils"
)

var (
	product       string
	dst           string
	configFile    string
	gofuncs       int
	freeze        string
	conf          utils.Config
	stages        string // coma seperated list possible
	ver           bool
	help          bool
	productFreeze string

	// will get filled on buildtime with:
	// go build -ldflags="-X 'main.builddate=$(date +%F)' -X 'main.branch=$(git branch --show-current)' -X 'main.commitId=$(git show -s --format=%H)'"
	// go build -ldflags="-X 'main.builddate=$(date +%F)' -X 'main.branch=$(git branch --show-current)' -X 'main.commitId=$(git show -s --format=%H)'"
	builddate string
	commitId  string
	branch    string
)

func init() {

	dateNow := time.Now().Format("2006-01-02")

	flag.StringVar(&product, "p", "", "product")
	flag.StringVar(&configFile, "f", "~/.makeFreeze.json", "config file to read")
	flag.IntVar(&gofuncs, "b", 4, "defines the number of go functions")
	flag.StringVar(&freeze, "n", dateNow, "how should the freeze get named")
	flag.StringVar(&stages, "c", "", "sets the current link for the given stage / environment like 'dev,int,stg,prod' also multiple stages are possible as coma seperated list!")
	flag.BoolVar(&ver, "v", false, "prints the version")
	flag.BoolVar(&help, "h", false, "prints this help")
	flag.Parse()

	if help {
		flag.CommandLine.Usage()
		os.Exit(2)
	}

	if ver {
		if builddate == "" {
			builddate = "builddate not set correctly during build time"
		}
		if commitId == "" || branch == "" {
			commitId = "branch, commitId not defined"
		}
		fmt.Println("Builddate:", builddate)
		fmt.Println("Branch, CommitId:", branch, commitId)
		fmt.Println("check for a newer version:  https://gitlab.com/pkruesi/makefreeze")
		os.Exit(2)
	}

	if product == "" {
		fmt.Println("ERROR: -p <product> is missing")
		flag.CommandLine.Usage()
		os.Exit(2)
	}

	if stages == "" {
		fmt.Println("ERROR: -c <stage> is missing")
		flag.CommandLine.Usage()
		os.Exit(2)
	}

	productFreeze = fmt.Sprintf("%s-%s", filepath.Clean(product), freeze)
}

type fsObject struct {
	info os.FileInfo
	path string
}

func (fsO fsObject) defineNewpath() string {
	return strings.Replace(fsO.path, conf.HeadDir, fmt.Sprintf("%s/%s", conf.RepoDirRoot, productFreeze), 1)
}

func worker(fsObjects chan fsObject, wg *sync.WaitGroup) {

	defer wg.Done()

	for fsO := range fsObjects {

		newpath := fsO.defineNewpath()

		if fsO.info.IsDir() {
			utils.CreateBaseDirs(newpath, fsO.info.Mode())
			log.Println("create Dir:", newpath)
		} else {
			newbase := filepath.Dir(newpath)
			utils.CreateBaseDirs(newbase, fsO.info.Mode())

			err := os.Link(fsO.path, newpath)
			if err != nil {
				if os.IsPermission(err) {
					log.Println("link:", err)
				}
			}
		}
	}
}

func main() {

	conf = utils.GetConfig(configFile)

	// should go into its own helper function
	if strings.HasPrefix(conf.HeadDir, "~/") {
		dirname, _ := os.UserHomeDir()
		conf.HeadDir = filepath.Join(dirname, conf.HeadDir[2:])
	}
	// should go into its own helper function
	if strings.HasPrefix(conf.RepoDirRoot, "~/") {
		dirname, _ := os.UserHomeDir()
		conf.RepoDirRoot = filepath.Join(dirname, conf.RepoDirRoot[2:])
	}

	headdir := filepath.Clean(conf.HeadDir)
	if isDir, err := utils.DirExists(headdir); !isDir {
		log.Fatal(err)
	}

	wg := sync.WaitGroup{}
	fsObjects := make(chan fsObject, 5)

	if utils.CheckFreezeExists(productFreeze, conf) {
		// if a productFreeze already exists we only make the current symlink
		utils.SetCurrentSymlink(stages, product, productFreeze, conf)
	}

	// run our worker for all found fsObjects
	for i := 0; i < gofuncs; i++ {
		wg.Add(1)
		go worker(fsObjects, &wg)
	}

	// filling our channel with files / paths to files
	go func() {
		err := filepath.Walk(headdir, func(path string, info os.FileInfo, err error) error {
			fsO := fsObject{info: info, path: path}
			if info.IsDir() {
				// if it is a directory we will create the directory and subdirectory immediality
				newpath := fsO.defineNewpath()
				utils.CreateBaseDirs(newpath, fsO.info.Mode().Perm())
			} else {
				// push fsObject to the chanel for further concurrent creation
				// all subdirectories should exist already
				fsObjects <- fsO
			}
			return err
		})

		if err != nil {
			log.Println(err)
		}

		// we need to close the channel
		close(fsObjects)
	}()

	wg.Wait()

	utils.SetCurrentSymlink(stages, product, productFreeze, conf)

}
