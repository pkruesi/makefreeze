# makeFreeze

Makes a freeze (copy) of a directory using os hardlinks instead a real copy.

## features

- lets you do some configs by a config file
- creates a "current" symlink to the newly created freeze

## constraints

- Works only on the same linux filesystem.
- Works only on linux at the moment (because sometimes the path gets built "manually")

## build the tool

You need to have go (golang) installed to build this tool

```bash
git clone https://gitlab.com/pkruesi/makefreeze.git
cd ./makefreeze
go build -ldflags="-X 'main.builddate=$(date +%F)' -X 'main.branch=$(git branch --show-current)' -X 'main.commitId=$(git show -s --format=%H)'"
```

## help

```bash
Usage of ./makeFreeze:
  -b int
     defines the number of go functions (default 4)
  -c string
     sets the current link for the given stage / environment like 'dev,int,stg,prod' also multiple stages are possible as coma seperated list!
  -f string
     config file to read (default "~/.makeFreeze.json")
  -h prints this help
  -n string
     how should the freeze get named (default "2022-06-30")
  -p string
     product
  -v prints the version
```

## examples

- config `makeFreeze.json`

  ```json
  {
    "HeadDir":"/data/repomirror/head",
    "RepoDirRoot":"/data/repomirror/repos"
  }
  ```

- create freeze with a single current symlink. Fore only one stage.

  ```bash
  ./makeFreeze -f makeFreeze.json -p mcs -c intEXAMPLE
  /data/repomirror/repos/current-mcs-intEXAMPLE  ->  /data/repomirror/repos/mcs-2022-06-04
  ```

- create freeze with multiple current symlinks. Fore multiple one stages.

  ```bash
  ./makeFreeze -f makeFreeze.json -p mcs -c intEXAMPLE,devEXAMPLE,mgmtEXAMPLE
  /data/repomirror/repos/current-mcs-intEXAMPLE  ->  /data/repomirror/repos/mcs-2022-06-04
  /data/repomirror/repos/current-mcs-devEXAMPLE  ->  /data/repomirror/repos/mcs-2022-06-04
  /data/repomirror/repos/current-mcs-mgmtEXAMPLE  ->  /data/repomirror/repos/mcs-2022-06-04
  ```

- create a current symlink to an already __existing__ freeze. Parameter `-n`

  ```bash
  ./makeFreeze -f makeFreeze.json -p mcs -n 2022-06-04 -c prodEXAMPLE
  /data/repomirror/repos/current-mcs-prodEXAMPLE  ->  /data/repomirror/repos/mcs-2022-06-04
  ```

## ToDo

- list already existing freezes to a given product
- list current symlink to a given product
- list curent symlinks to all products

## Licinse

This software is published under the [MIT license](https://choosealicense.com/licenses/mit/)

### Licensetext

```text
Copyright 2022 pkruesi

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
```
